package de.jm.sudoku.model;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CellImplTest {

    @Test
    public void testIsFix() {
        Cell cell = new CellImpl(4);
        assertFalse(cell.isFix());
        cell.setValue(3);
        assertTrue(cell.isFix());

        cell = new CellImpl(4);
        cell.removeValue(0);
        cell.removeValue(1);
        cell.removeValue(2);
        assertTrue(cell.isFix());
    }

    @Test
    public void getPossibleValues() {
        int size = 4;
        Cell cell = new CellImpl(size);
        List<Integer> values = cell.getPossibleValues();
        assertEquals(size, values.size());

        for (int i = 0; i < size; i++) {
            assertEquals(i, values.get(i).intValue());
        }
    }

    @Test
    public void setValue() {
        Cell cell = new CellImpl(4);
        cell.setValue(3);
        assertTrue(cell.isFix());
    }

    @Test
    public void removeValue() {
        int size = 4;
        Cell cell = new CellImpl(size);

        cell.removeValue(0);
        assertEquals("234", cell.toString());
        cell.removeValue(3);
        assertEquals("23", cell.toString());
        cell.removeValue(0);
        assertEquals("23", cell.toString());
        cell.removeValue(size);
        assertEquals("23", cell.toString());
    }

    @Test
    public void testToString() {
        Cell cell = new CellImpl(4);
        assertEquals("1234", cell.toString());
        cell = new CellImpl(12);
        assertEquals("123456789abc", cell.toString());
    }
}