package de.jm.sudoku.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class SudokuImplTest {

    @Test
    public void setCell() {
        Cell cell;
        Sudoku sudoku = getSodoku();
        cell = new CellImpl(1,1,1, 4);
        cell.setValue(10);
        sudoku.setCell(cell);

        assertEquals(cell.toString(), sudoku.getRow(cell).get(cell.getRow()).toString());
        assertEquals(cell.toString(), sudoku.getCol(cell).get(cell.getCol()).toString());

        System.out.println(sudoku);
        System.out.println(sudoku.getRow(cell));
        System.out.println(sudoku.getCol(cell));
        System.out.println(sudoku.getArea(cell));
    }

    @Test
    public void getRow() {
        Cell cell;
        Sudoku sudoku = getSodoku();
        cell = new CellImpl(0,0,0, 3);
        System.out.println(sudoku);
        System.out.println();
        System.out.println(sudoku.getRow(cell));

    }

    @Test
    public void getCol() {
        Cell cell;
        Sudoku sudoku = getSodoku();
        cell = new CellImpl(0,0,0, 3);
        System.out.println(sudoku);
        System.out.println();
        System.out.println(sudoku.getCol(cell));
    }

    @Test
    public void getArea() {
        Cell cell;
        Sudoku sudoku = getSodoku();
        System.out.println(sudoku);
        System.out.println();
        System.out.println(sudoku.getArea(new CellImpl(0,1,0, 3)));
        System.out.println(sudoku.getArea(new CellImpl(0,1,1, 3)));
        System.out.println(sudoku.getArea(new CellImpl(0,1,2, 3)));
    }

    @Test
    public void getAllRows() {
    }

    @Test
    public void getAllCols() {
    }

    @Test
    public void getAllAreas() {
    }

    @Test
    public void testToString() {
        Sudoku sudoku = getSodoku();
        System.out.println(sudoku);
    }

    private Sudoku getSodoku() {
        Cell cell;
        Sudoku sudoku = new SudokuImpl(3);

        cell = new CellImpl(0,0,0,1);
        sudoku.setCell(cell);
        cell = new CellImpl(0,1,1,2);
        sudoku.setCell(cell);
        cell = new CellImpl(0,2,2,3);
        sudoku.setCell(cell);

        cell = new CellImpl(1,0,0,3);
        sudoku.setCell(cell);
        cell = new CellImpl(1,1,1,1);
        sudoku.setCell(cell);
        cell = new CellImpl(1,2,2,2);
        sudoku.setCell(cell);

        cell = new CellImpl(2,0,0,1);
        sudoku.setCell(cell);
        cell = new CellImpl(2,1,1,3);
        sudoku.setCell(cell);
        cell = new CellImpl(2,2,2,1);
        sudoku.setCell(cell);

        return sudoku;
    }

    @Test
    public void readSudoku() {

    }
}