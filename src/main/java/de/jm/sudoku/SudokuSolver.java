package de.jm.sudoku;

import de.jm.sudoku.interfaces.Sudoku;
import de.jm.sudoku.strategie.BruteForceStrategy;
import de.jm.sudoku.strategie.FixPosStrategy;
import de.jm.sudoku.strategie.FixValueStrategy;
import de.jm.sudoku.strategie.Strategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// http://sudokuspoiler.azurewebsites.net/IrregularSudoku/IrregularSudoku12
// https://en.wikipedia.org/wiki/Sudoku_solving_algorithms
// https://en.wikipedia.org/wiki/Glossary_of_Sudoku

// https://www.codeproject.com/Articles/34403/Sudoku-as-a-CSP
// https://community.oracle.com/docs/DOC-983413

// Strategies
// http://www.sudokuwiki.org/jigsaw.htm

public class SudokuSolver {
    private List<Strategy> strategies = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        SudokuSolver solver = new SudokuSolver();
        //Sudoku sudoku = SudokuFaktory.getStandard();
        //Sudoku sudoku = SudokuFaktory.getStandard("data/110.sdk");
        Sudoku sudoku = SudokuFaktory.getStandard("data/153.sdk");
        //Sudoku sudoku = SudokuFaktory.getStandard("data/202.sdk");
        if (solver.solve(sudoku)) {
            System.out.println("Solved");
        } else {
            System.out.println("Not solved");
        }
        SudokuView.printValues1(sudoku);
    }

    public SudokuSolver() {
        strategies.add(new FixValueStrategy());
        strategies.add(new FixPosStrategy());
        strategies.add(new BruteForceStrategy());
    }

    private boolean solve(Sudoku sudoku) {

        boolean changedByAnyStrategy;
        do {
            changedByAnyStrategy = false;
            for (Strategy strategy : strategies) {
                boolean changed;
                System.out.println(strategy.getClass().getName());
                do {
                    changed = strategy.execute(sudoku);
                    changedByAnyStrategy |= changed;
                } while (changed);

                if (sudoku.isSolved()) {
                    return true;
                } else {
                    SudokuView.printValues(sudoku);
                }
            }
        } while (changedByAnyStrategy);

        return false;
    }
}

