package de.jm.sudoku;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public class Coordinates {
    public int x;
    public int y;

    public Coordinates(int theX, int theY) {
        x = theX;
        y = theY;
    }
}
