package de.jm.sudoku;

import de.jm.sudoku.interfaces.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public class FieldImpl implements Field {
    private String possibleValues;
    private List<Character> values;
    private int x;
    private int y;

    private FieldImpl() {}

    FieldImpl(String possibleValues, int theX, int theY) {
        this.possibleValues = possibleValues;
        x = theX;
        y = theY;
        init();
    }

    private void init() {
        values = new ArrayList<>();

        for (char c : possibleValues.toCharArray()) {
            values.add(c);
        }
    }

    public boolean isFix() {
        return values.size() == 1;
    }

    public void setFixValue(char theValue) {
        values.clear();
        values.add(theValue);
    }

    public char getFixValue() {
        if (isFix()) {
            return values.get(0);
        } else {
            return '?';
        }
    }

    @Override
    public boolean removeValue(char theValue) {
        return values.remove(Character.valueOf(theValue));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public List<Character> getValues() {
        return values;
    }

    @Override
    public String getPossibleValues() {
        return possibleValues;
    }
}

