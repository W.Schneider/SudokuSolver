package de.jm.sudoku;

import de.jm.sudoku.interfaces.Sudoku;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public class SudokuFaktory {
    private static Map<Character, List<Coordinates>> areas = new HashMap<Character, List<Coordinates>>();
    private static String areasValues =
            "111 222 333\n" +
                    "111 222 333\n" +
                    "111 222 333\n" +

                    "444 555 666\n" +
                    "444 555 666\n" +
                    "444 555 666\n" +

                    "777 888 999\n" +
                    "777 888 999\n" +
                    "777 888 999\n";

    public static void main(String[] args) throws IOException {
        getStandard();
    }

    public static Sudoku getStandard(String filename) throws IOException {
        SudokuFaktory sudokuFaktory = new SudokuFaktory();
        Sudoku sudoku = new SudokuImpl("123456789");

        sudokuFaktory.readAreas(new StringReader(areasValues));
        for (List<Coordinates> area : areas.values()) {
            sudoku.addArea(area);
        }

        sudokuFaktory.readValues(new FileReader(filename), sudoku);

        //SudokuView.printRows(sudoku);
        //SudokuView.printCols(sudoku);
        //SudokuView.printAreas(sudoku);
        SudokuView.printFixValues(sudoku);
        return sudoku;

    }

    public static Sudoku getStandard() throws IOException {
        SudokuFaktory sudokuFaktory = new SudokuFaktory();

        Sudoku sudoku = new SudokuImpl("123456789");


        sudokuFaktory.readAreas(new StringReader(areasValues));
        for (List<Coordinates> area : areas.values()) {
            sudoku.addArea(area);
        }

        String values =
                "**9 **5 8*7\n" +
                "6*8 *94 2**\n" +
                "**2 3** 49*\n" +

                "243 *6* *8*\n" +
                "*** *** ***\n" +
                "*5* *4* 932\n" +

                "*25 **3 7**\n" +
                "**4 91* 5*8\n" +
                "8*7 2** 3**\n";

        sudokuFaktory.readValues(new StringReader(values), sudoku);

        //SudokuView.printRows(sudoku);
        //SudokuView.printCols(sudoku);
        //SudokuView.printAreas(sudoku);
        SudokuView.printFixValues(sudoku);
        return sudoku;
    }


    private void readAreas(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        int x = 0;
        int y = 0;

        while ((line = bufferedReader.readLine()) != null) {
            for (char c : line.toCharArray()) {
                if (charValidForArea(c)) {
                    List<Coordinates> area = getArea(c);
                    area.add(new Coordinates(x, y));
                    x++;
                }
            }
            y++;
            x=0;
        }
    }

    private List<Coordinates> getArea(char c) {
        return areas.computeIfAbsent(c, k -> new ArrayList<Coordinates>());
    }

    private boolean charValidForArea(char c) {
        return (c != ' ');
    }

    private void readValues(Reader reader, Sudoku sudoku) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        int x = 0;
        int y = 0;

        while ((line = bufferedReader.readLine()) != null) {
            for (char c : line.toCharArray()) {
                if (charValidForField(c)) {
                    if (c != '*') {
                        sudoku.setField(x, y, c);
                    }
                    x++;
                }
            }
            if (!line.trim().isEmpty()) {
                y++;
            }
            x=0;
        }
    }

    private boolean charValidForField(char c) {
        return c != ' ';
    }
}
