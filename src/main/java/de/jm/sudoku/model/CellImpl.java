package de.jm.sudoku.model;

import java.util.ArrayList;
import java.util.List;

public class CellImpl implements Cell {
    private int row;
    private int col;
    private int area;
    private int maxStringSize;

    private List<Integer> possibleValues;
    private final static char[] VIEW = "123456789abcdefg".toCharArray();

    public CellImpl(int size) {
        possibleValues = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            possibleValues.add(i);
        }
        possibleValues = getPossibleValues(size);
        maxStringSize = possibleValues.toString().length();
    }

    public CellImpl(int row, int col, int area, int size) {
        this(size);

        this.row = row;
        this.col = col;
        this.area = area;

    }

    private static List<Integer> getPossibleValues(int size) {
        List<Integer> values = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            values.add(i);
        }

        return values;
    }

    @Override
    public boolean isFix() {
        return possibleValues.size() == 1;
    }

    @Override
    public int getRow() {
        return row;
    }

    @Override
    public void setRow(int row) {
        this.row = row;
    }

    @Override
    public int getCol() {
        return col;
    }

    @Override
    public void setCol(int col) {
        this.col = col;
    }

    @Override
    public int getArea() {
        return area;
    }

    @Override
    public void setArea(int area) {
        this.area = area;
    }

    @Override
    public List<Integer> getPossibleValues() {
        return possibleValues;
    }

    @Override
    public void setValue(int value) {
        possibleValues.clear();
        possibleValues.add(value);
    }

    @Override
    public void removeValue(int value) {
        possibleValues.remove(Integer.valueOf(value));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int p : possibleValues) {
            builder.append(VIEW[p]);
        }
        builder.append(',').append(area);
        return builder.toString();
    }
}

