package de.jm.sudoku.model;

import java.util.ArrayList;

public class Cells extends ArrayList<Cell> {
    public Cells(int i) {
        super(i);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Cell cell : this) {
            builder.append(cell).append(" ");
        }
        return builder.toString();
    }
}



