package de.jm.sudoku.model;

import java.util.ArrayList;
import java.util.List;



public class SudokuImpl implements Sudoku {

    private List<Cells> rows;
    private List<Cells> cols;
    private List<Cells> areas;

    public SudokuImpl(int size) {
        initializeCells(size);
    }

    public SudokuImpl(Cells allCells, int size) {
        this(size);

        for (Cell cell : allCells) {
            setCell(cell);
        }
    }

    @Override
    public void setCell(Cell cell) {
        rows.get(cell.getRow()).set(cell.getCol(), cell);
        cols.get(cell.getCol()).set(cell.getRow(), cell);
        int index = getAreaIndex(cell);
        if (index == -1) {
            areas.get(cell.getArea()).add(cell);
        } else {
            areas.get(cell.getArea()).set(index, cell);
        }
    }

    private int getAreaIndex(Cell cell) {
        Cells area = areas.get(cell.getArea());
        int max = area.size();
        for (int i = 0; i < max; i++) {
            Cell c = area.get(i);
            if (c.getRow() == cell.getRow() && c.getCol() == cell.getCol()) {
                return i;
            }
        }
        return -1;
    }

    private void initializeCells(int size) {
        rows = new ArrayList<>(size);
        cols = new ArrayList<>(size);
        areas = new ArrayList<>(size);

        for (int i = 0; i < size; i++) {
            rows.add(getEmptyCells(size));
            areas.add(new Cells(size));
        }

        for (int i = 0; i < size; i++) {
            cols.add(getInitalCol(i, size));
        }
    }

    private Cells getInitalCol(int i, int size) {
        Cells col = new Cells(size);

        for (Cells row : rows) {
            col.add(row.get(i));
        }

        return col;
    }

    private Cells getEmptyCells(int size) {
        Cells cells = new Cells(size);
        for (int i = 0; i < size; i++) {
            cells.add(new CellImpl(size));
        }
        return cells;
    }

    @Override
    public Cells getRow(Cell cell) {
        return rows.get(cell.getCol());
    }

    @Override
    public Cells getCol(Cell cell) {
        return cols.get(cell.getRow());
    }

    @Override
    public Cells getArea(Cell cell) {
        return areas.get(cell.getArea());
    }

    @Override
    public List<Cells> getAllRows() {
        return rows;
    }

    @Override
    public List<Cells> getAllCols() {
        return cols;
    }

    @Override
    public List<Cells> getAllAreas() {
        return areas;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        int max = getMaxSize();

        for (Cells row : rows) {
            for (Cell cell : row) {
                String s = String.format("%-" + max + "s", cell.toString());
                builder.append(s).append(" ");
            }
            builder.append('\n');
        }

        return builder.toString();
    }

    int getMaxSize() {
        int max = 0;
        for (Cells row : rows) {
            for (Cell cell : row) {
                int size = cell.getPossibleValues().size();
                if (size > max) {
                    max = size;
                }
            }
        }
        return max;
    }
}



