package de.jm.sudoku.model;

import java.util.List;

public interface Cell {
    int getRow();
    void setRow(int row);

    int getCol();
    void setCol(int col);

    int getArea();
    void setArea(int area);

    List<Integer> getPossibleValues();

    void setValue(int value);
    void removeValue(int value);

    boolean isFix();

}
