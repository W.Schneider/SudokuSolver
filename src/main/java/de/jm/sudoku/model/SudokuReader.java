package de.jm.sudoku.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SudokuReader {
    public static void main(String[] args) throws IOException {
        Sudoku sudoku = read("73.sdk");
        System.out.println(sudoku);
    }

    public static Sudoku read(String filename) throws IOException {
        String dir = "/home/johann/git/SudokuSolver/data";

        //Stream<String> lines = Files.lines(Paths.get(dir, filename));
        //lines.forEach(line -> System.out.println(line));

        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(dir, filename)));

        List<String> lines = new ArrayList<>();
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }

        int size = getSize(lines);

        Sudoku sudoku = new SudokuImpl(size);
        final String VIEW = "123456789abcdefg";
        int row = 0;
        int col = 0;
        int areaBase = 0;
        int areaMax = 0;

        for (String l : lines) {
            String trimmedLine = l.trim();

            if (trimmedLine.isEmpty()) {
                areaBase = areaMax + 1;
                continue;
            }

            col = 0;

            int area = areaBase;

            Cell cell;
            for (char c : l.toCharArray()) {
                if (c == ' ') {
                    area++;
                    if (area > areaMax) {
                        areaMax = area;
                    }
                    continue;
                }

                cell = new CellImpl(row, col, area, size);
                if (c != '*') {
                    cell.setValue(VIEW.indexOf(c));
                }
                sudoku.setCell(cell);
                col++;
            }

            row++;
        }


        return sudoku;

    }

    private static int getSize(List<String> lines) {
        if (lines == null || lines.isEmpty()) {
            return 0;
        }

        String line = lines.get(0);
        return line.replaceAll(" ", "").length();
    }

}
