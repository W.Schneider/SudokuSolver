package de.jm.sudoku.model;

import java.util.List;

public interface Sudoku {

    Cells getRow(Cell cell);
    Cells getCol(Cell cell);
    Cells getArea(Cell cell);

    List<Cells> getAllRows();
    List<Cells> getAllCols();
    List<Cells> getAllAreas();

    void setCell(Cell cell);
}