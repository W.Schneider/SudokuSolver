package de.jm.sudoku.interfaces;

import java.util.List;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public interface Area {
    List<Field> getFields();

    boolean contains(Field field);

    Field getField(int x);
    void addField(Field field);
}
