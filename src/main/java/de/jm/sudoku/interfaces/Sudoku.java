package de.jm.sudoku.interfaces;

import de.jm.sudoku.Coordinates;

import java.util.LinkedList;
import java.util.List; /**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public interface Sudoku {
    Field getField(int x, int y);
    void setField(int x, int y, char value);

    Area getArea(Field field);
    Area getRowArea(Field field);
    Area getColArea(Field field);
    Area getRowArea(int y);
    Area getColArea(int x);
    LinkedList<Field> getNotFixedFields();

    void addArea(List<Coordinates> coordinates);

    List<Area> getRows();
    List<Area> getCols();
    List<Area> getAreas();

    boolean isSolved();

    String getPossibleValues();
}

