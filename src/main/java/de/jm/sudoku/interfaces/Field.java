package de.jm.sudoku.interfaces;

import java.util.List;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public interface Field {
    int getX();
    int getY();

    boolean isFix();
    void setFixValue(char theValue);
    char getFixValue();

    /**
     *
     * @param theValue to remove
     * @return true if the field state was changed
     */
    boolean removeValue(char theValue);

    List<Character> getValues();

    String getPossibleValues();
}

