package de.jm.sudoku;

import de.jm.sudoku.interfaces.Area;
import de.jm.sudoku.interfaces.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public class AreaImpl implements Area {
    private List<Field> fields;
    private int size;

    public AreaImpl(int theSize) {
        size = theSize;
        fields = new ArrayList<Field>(size);
    }

    public List<Field> getFields() {
        return fields;
    }

    public boolean contains(Field field) {
        return fields.contains(field);
    }

    public Field getField(int x) {
        return fields.get(x);
    }

    public void addField(Field field) {
        fields.add(field);
    }
}
