package de.jm.sudoku;

import de.jm.sudoku.interfaces.Area;
import de.jm.sudoku.interfaces.Field;
import de.jm.sudoku.interfaces.Sudoku;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 20.10.2017.
 */
public class SudokuImpl implements Sudoku {
    private List<Area> rows;
    private List<Area> areas;
    private List<Area> cols;
    private int size;
    private String possibleValues;

    public SudokuImpl() {
        this("123456789");
    }

    public SudokuImpl(String possibleValues) {
        setPossibleValues(possibleValues);
        areas = new ArrayList<>(size);
    }

    private Area getArea(Field field, List<Area> areas) {
        for (Area area : areas) {
            if (area.contains(field)) {
                return area;
            }
        }

        return null;
    }

    @Override
    public Area getArea(Field field) {
        return getArea(field, areas);
    }

    @Override
    public Field getField(int x, int y) {
        return getRowArea(y).getField(x);
    }

    @Override
    public void setField(int x, int y, char value) {
        getRowArea(y).getField(x).setFixValue(value);
    }

    @Override
    public Area getRowArea(Field field) {
        return getArea(field, rows);
    }

    @Override
    public Area getColArea(Field field) {
        return getArea(field, cols);
    }

    @Override
    public Area getRowArea(int y) {
        return rows.get(y);
    }

    @Override
    public Area getColArea(int x) {
        return cols.get(x);
    }

    @Override
    public LinkedList<Field> getNotFixedFields() {
        LinkedList<Field> notFixedFields = new LinkedList<>();

        for (Area row : rows) {
            for (Field field : row.getFields()) {
                if (!field.isFix()) {
                    notFixedFields.add(field);
                }
            }
        }

        return notFixedFields;
    }

    private void setPossibleValues(String possibleValues) {
        this.possibleValues = possibleValues;
        size = possibleValues.length();
        createRows();
        createCols();
    }


    private void createCols() {
        cols = new ArrayList<>(size);
        for (int x = 0; x < size; x++) {
            Area area = new AreaImpl(size);
            for (int y = 0; y < size; y++) {
                area.addField(getRowArea(y).getField(x));
            }
            cols.add(area);
        }
    }

    private void createRows() {
        rows = new ArrayList<>(size);
        for (int y = 0; y < size; y++) {
            Area area = new AreaImpl(size);
            for (int x = 0; x < size; x++) {
                area.addField(new FieldImpl(possibleValues, x, y));
            }
            rows.add(area);
        }
    }

    @Override
    public void addArea(List<Coordinates> list) {
        Area area = new AreaImpl(list.size());
        for (Coordinates c : list) {
            area.addField(getField(c.x, c.y));
        }
        areas.add(area);
    }

    @Override
    public List<Area> getRows() {
        return rows;
    }

    @Override
    public List<Area> getCols() {
        return cols;
    }

    @Override
    public List<Area> getAreas() {
        return areas;
    }

    @Override
    public boolean isSolved() {
        for (Area row : getRows()) {
            for (Field field : row.getFields()) {
                if (!field.isFix()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public String getPossibleValues() {
        return possibleValues;
    }
}

