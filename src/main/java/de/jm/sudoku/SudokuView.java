package de.jm.sudoku;

import de.jm.sudoku.interfaces.Area;
import de.jm.sudoku.interfaces.Field;
import de.jm.sudoku.interfaces.Sudoku;


public class SudokuView {
    public static void printRows(Sudoku sudoku) {
        System.out.println("Rows:");
        for (Area row : sudoku.getRows()) {
            printArea(row);
        }
    }

    public static void printCols(Sudoku sudoku) {
        System.out.println("Cols:");
        for (Area col : sudoku.getCols()) {
            printArea(col);
        }
    }

    public static void printAreas(Sudoku sudoku) {
        System.out.println("Areas:");
        for (Area area : sudoku.getAreas()) {
            printArea(area);
        }
    }

    private static void printArea(Area area) {
        for (Field field: area.getFields()) {
            System.out.print("(" + field.getX() + ", " + field.getY() + ") ");
        }
        System.out.println();
        System.out.println();
    }

    static void printValues(Sudoku sudoku) {
        System.out.println("Values:");
        for (Area row : sudoku.getRows()) {
            for (Field field : row.getFields()) {
                printFieldValues(field);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    static void printValues1(Sudoku sudoku) {
        System.out.println("Values:");
        StringBuilder builder = new StringBuilder();

        for (Area row : sudoku.getRows()) {
            for (Field field : row.getFields()) {
                builder.append(getFirstRow(field));
                builder.append(" ");
            }
            builder.append("\n");

            for (Field field : row.getFields()) {
                builder.append(getSecondRow(field));
                builder.append(" ");
            }
            builder.append("\n");

            for (Field field : row.getFields()) {
                builder.append(getThirdRow(field));
                builder.append(" ");
            }
            builder.append("\n\n");
        }

        System.out.println(builder);
    }

    private static String getFirstRow(Field field) {
        StringBuilder row = new StringBuilder();
        String possibleValue = field.getPossibleValues();
        for (int i = 0; i < 3; i++) {
            char value = possibleValue.charAt(i);
            if (field.getValues().contains(value)) {
                row.append(value);
            } else {
                row.append("*");
            }
        }
        return row.toString();
    }

    private static String getSecondRow(Field field) {
        StringBuilder row = new StringBuilder();
        String possibleValue = field.getPossibleValues();
        for (int i = 3; i < 6; i++) {
            char value = possibleValue.charAt(i);
            if (field.getValues().contains(value)) {
                row.append(value);
            } else {
                row.append("*");
            }
        }
        return row.toString();
    }

    private static String getThirdRow(Field field) {
        StringBuilder row = new StringBuilder();
        String possibleValue = field.getPossibleValues();
        for (int i = 6; i < 9; i++) {
            char value = possibleValue.charAt(i);
            if (field.getValues().contains(value)) {
                row.append(value);
            } else {
                row.append("*");
            }
        }
        return row.toString();
    }

    static void printFixValues(Sudoku sudoku) {
        System.out.println("Values:");
        for (Area row : sudoku.getRows()) {
            for (Field field : row.getFields()) {
                if (field.isFix()) {
                    System.out.print(field.getFixValue());
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    private static void printFieldValues(Field field) {
        for (Character value : field.getValues()) {
            System.out.print(value);
        }
    }
}

