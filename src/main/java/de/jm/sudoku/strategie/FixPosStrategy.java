package de.jm.sudoku.strategie;

import de.jm.sudoku.interfaces.Area;
import de.jm.sudoku.interfaces.Field;
import de.jm.sudoku.interfaces.Sudoku;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang Schneider on 23.10.2017.
 */
public class FixPosStrategy implements Strategy {
    private Sudoku sudoku;
    @Override
    public boolean execute(Sudoku sudokuToSolve) {
        sudoku = sudokuToSolve;
        boolean changed = false;
        while (doIt()) {
            changed = true;
        }
        return changed;
    }

    private boolean doIt() {
        boolean changed = proceedArea(sudoku.getRows());
        changed |= proceedArea(sudoku.getCols());
        changed |= proceedArea(sudoku.getAreas());
        return changed;
    }

    private boolean proceedArea(List<Area> areas) {
        boolean changed = false;
        String possibleValues = sudoku.getPossibleValues();

        for (Area area : areas) {
            for (char c : possibleValues.toCharArray()) {
                Field fixPosField = getFixPosField(area, c);
                if (fixPosField != null && !fixPosField.isFix()) {
                    sudoku.setField(fixPosField.getX(), fixPosField.getY(), c);
                    changed = true;
                }
            }
        }

        return changed;
    }

    private Field getFixPosField(Area area, char value) {
        List<Field> fields = new ArrayList<>();

        for (Field field : area.getFields()) {
            if (field.getValues().contains(value)) {
                fields.add(field);
            }
        }

        if (fields.size() == 1) {
            return fields.get(0);
        } else {
            return null;
        }
    }
}

