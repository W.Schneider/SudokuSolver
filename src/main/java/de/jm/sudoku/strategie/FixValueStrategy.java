package de.jm.sudoku.strategie;

import de.jm.sudoku.interfaces.Area;
import de.jm.sudoku.interfaces.Field;
import de.jm.sudoku.interfaces.Sudoku;

public class FixValueStrategy implements Strategy {
    private Sudoku sudoku;

    @Override
    public boolean execute(Sudoku theSudoku) {
        sudoku = theSudoku;
        boolean changed = false;
        while (doIt()) {
            changed = true;
        }
        return changed;
    }

    private boolean doIt() {
        boolean changed = false;
        for (Area row : sudoku.getRows()) {
            for (Field field : row.getFields()) {
                if (field.isFix()) {
                    changed |= removeValues(field);
                }
            }
        }
        return changed;
    }

    private boolean removeValues(Field field) {
        boolean changed = removeValuesFromArea(sudoku.getRowArea(field), field);
        changed |= removeValuesFromArea(sudoku.getColArea(field), field);
        changed |= removeValuesFromArea(sudoku.getArea(field), field);
        return changed;
    }

    private boolean removeValuesFromArea(Area area, Field valueVield) {
        boolean changed = false;
        for (Field field : area.getFields()) {
            for (Character value : valueVield.getValues()) {
                if (field != valueVield) {
                    changed |= field.removeValue(value);
                }
            }
        }
        return changed;
    }
}
