package de.jm.sudoku.strategie;

import de.jm.sudoku.interfaces.Sudoku;

public interface Strategy {
    boolean execute(Sudoku sudoku);
}
