package de.jm.sudoku.strategie;

import de.jm.sudoku.interfaces.Field;
import de.jm.sudoku.interfaces.Sudoku;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by Wolfgang Schneider on 27.10.2017.
 */
public class BruteForceStrategy implements Strategy {
    private Sudoku sudoku;
    @Override
    public boolean execute(Sudoku sudoku) {
        this.sudoku = sudoku;
        LinkedList<Field> notFixFields = sudoku.getNotFixedFields();
        proceedField(notFixFields.listIterator());

        return false;
    }

    private void proceedField(ListIterator<Field> iter) {
        if (sudoku.isSolved()) {
            return;
        }

        if (iter.hasNext()) {
            Field field = iter.next();
            for (char c : field.getValues()) {
                field.setFixValue(c);
                proceedField(iter);
            }
        }
    }

}
